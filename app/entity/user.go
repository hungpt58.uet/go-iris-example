package entity

import "gorm.io/gorm"

type UserStatus uint

const (
	Pending   UserStatus = 1
	InProcess UserStatus = 2
	Complete  UserStatus = 3
)

type User struct {
	gorm.Model
	ID     uint       `gorm:"primaryKey;column:id;not null;autoIncrement"`
	Name   string     `gorm:"column:name;not null"`
	Age    uint       `gorm:"column:age;not null"`
	PW     string     `gorm:"column:pw;not null"`
	Status UserStatus `gorm:"column:status;not null"`
}

func (user User) TableName() string {
	return "user_tbl"
}
