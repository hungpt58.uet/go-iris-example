package entity

import "gorm.io/gorm"

type Classroom struct {
	gorm.Model
	ID   int    `gorm:"primaryKey;column:id;not null;autoIncrement"`
	Name string `gorm:"column:name;not null"`
}

func (user Classroom) TableName() string {
	return "classroom_tbl"
}
