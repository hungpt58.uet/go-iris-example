package main

import (
	"github.com/go-playground/validator/v10"
	"github.com/kataras/iris/v12"
	"go-iris-example/app/api/controller"
	"go-iris-example/app/api/middleware"
	"go-iris-example/app/config"
	"go-iris-example/app/repository"
	"go-iris-example/app/service"
)

var (
	db             = config.SetupDB()
	userRepository = repository.NewUserRepository(db)
	clRepository   = repository.NewClassroomRepository(db)
	userService    = service.NewUserService(userRepository, clRepository)
	userController = controller.NewUserController(userService)
)

func main() {
	//-------------/ AREA: Create app instance /-------------//
	app := iris.New()
	app.Use(iris.Compression)
	middleware.HandleError(app)
	app.Validator = validator.New()

	//-------------/ AREA: Routes /-------------//
	app.Get("/", func(ctx iris.Context) {
		ctx.Text("OK")
	})

	mainRoute := app.Party("/api/v1")
	{
		mainRoute.Get("/users", userController.GetAllUsers)
		mainRoute.Get("/users-with-classrooms", userController.GetAllWithClassroom)
		mainRoute.Get("/users/{id:uint}", userController.GetUserById)
		mainRoute.Post("/users", userController.CreateNewUser)
		mainRoute.Delete("/users/{id:uint}", userController.DeleteUserById)
	}

	//-------------/ AREA: Start app /-------------//
	app.Listen(":8080")
}
