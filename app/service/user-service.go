package service

import (
	"go-iris-example/app/api/data/res"
	"go-iris-example/app/entity"
	"go-iris-example/app/repository"
	"sync"
)

// -----------------[ AREA: Definitions ]-----------------//
type UserService interface {
	ResolveAll() []entity.User
	ResolveById(ID uint) (*entity.User, error)
	Create(user *entity.User) error
	DeleteById(ID uint) error
	ResolveAllWithClassroom() []res.UserClassJs
}

type userService struct {
	userRepository      repository.UserRepository
	classroomRepository repository.ClassroomRepository
}

func NewUserService(
	userRepository repository.UserRepository,
	classroomRepository repository.ClassroomRepository,
) UserService {
	return &userService{userRepository, classroomRepository}
}

// -----------------[ AREA: Implementations ]-----------------//
func (us userService) ResolveAll() []entity.User {
	return us.userRepository.FindAll()
}

func (us userService) ResolveById(ID uint) (*entity.User, error) {
	return us.userRepository.FindById(ID)
}

func (us userService) Create(user *entity.User) error {
	return us.userRepository.Store(user)
}

func (us userService) DeleteById(ID uint) error {
	return us.userRepository.DeleteById(ID)
}

func (us userService) ResolveAllWithClassroom() []res.UserClassJs {
	var users []entity.User
	var classrooms []entity.Classroom

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		users = us.ResolveAll()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		classrooms = us.classroomRepository.FindAll()
	}()
	wg.Wait()

	return res.ToUserClassJs(users, classrooms)
}
