package util

import "fmt"

type EntityNotFoundErr struct {
	Message string
}

func (err *EntityNotFoundErr) Error() string {
	return fmt.Sprintf(err.Message)
}
