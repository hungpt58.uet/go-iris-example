package req

import "go-iris-example/app/entity"

// ---------------/ AREA: Struct definitions /---------------//
type CreateUser struct {
	Name string `json:"name" validate:"required"`
	Age  uint   `json:"age" validate:"required,min=3,max=12"`
	PW   string `json:"pw" validate:"required"`
}

type UpdateUser struct {
	Name string
	Age  int
}

// ---------------/ AREA: Methods /---------------//
func (req CreateUser) ToEntity() entity.User {
	return entity.User{
		Name: req.Name, Age: req.Age, PW: req.PW, Status: entity.Pending,
	}
}
