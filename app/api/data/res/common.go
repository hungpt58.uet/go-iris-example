package res

type ErrorJs struct {
	Success bool   `json:"success"`
	Error   string `json:"error"`
}

type SuccessJs[T any] struct {
	Success bool `json:"success"`
	Results *[]T `json:"results,omitempty"`
	Result  *T   `json:"result,omitempty"`
}

func Success[T any](data T) SuccessJs[T] {
	return SuccessJs[T]{
		Success: true,
		Result:  &data,
	}
}

func SuccessL[T any](data []T) SuccessJs[T] {
	return SuccessJs[T]{
		Success: true,
		Results: &data,
	}
}

func Failed(message string) ErrorJs {
	return ErrorJs{
		Success: false,
		Error:   message,
	}
}
