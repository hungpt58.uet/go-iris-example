package res

import "go-iris-example/app/entity"

// ----------------/ AREA: UserJs /-----------------//
type UserJs struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
	Age  uint   `json:"age"`
}

func ToUsersJs(users []entity.User) []UserJs {
	userJsList := make([]UserJs, len(users))

	for i, user := range users {
		userJsList[i] = ToUserJs(user)
	}

	return userJsList
}

func ToUserJs(user entity.User) UserJs {
	return UserJs{
		user.ID, user.Name, user.Age,
	}
}

// ----------------/ AREA: UserWithClassroomJs /-----------------//
type ClassroomJs struct {
	Name string `json:"name"`
}

type UserClassJs struct {
	ID         uint          `json:"id"`
	Name       string        `json:"name"`
	Age        uint          `json:"age"`
	Classrooms []ClassroomJs `json:"classrooms"`
}

func ToUserClassJs(users []entity.User, classrooms []entity.Classroom) []UserClassJs {
	var usersJs = make([]UserClassJs, 0, len(users))
	var classroomsJs = make([]ClassroomJs, 0, len(classrooms))

	for _, clr := range classrooms {
		classroomsJs = append(classroomsJs, ClassroomJs{Name: clr.Name})
	}
	for _, user := range users {
		usersJs = append(usersJs, UserClassJs{
			ID:         user.ID,
			Name:       user.Name,
			Age:        user.Age,
			Classrooms: classroomsJs,
		})
	}

	return usersJs
}
