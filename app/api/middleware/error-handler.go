package middleware

import (
	"github.com/kataras/iris/v12"
	"go-iris-example/app/api/data/res"
	"go-iris-example/app/util"
)

func HandleError(app *iris.Application) {
	app.OnErrorCode(iris.StatusNotFound, notFound)
	app.OnErrorCode(iris.StatusBadRequest, badRequest)
	app.OnErrorCode(iris.StatusInternalServerError, internalError)
}

func CatchError(error error, ctx iris.Context) {
	switch error.(type) {
	case *util.EntityNotFoundErr:
		ctx.StatusCode(iris.StatusNotFound)
		ctx.JSON(res.Failed(error.Error()))
		return
	default:
		ctx.StatusCode(iris.StatusInternalServerError)
	}
}

func notFound(ctx iris.Context) {
	error := res.ErrorJs{Success: false, Error: "Resource is not found"}
	ctx.JSON(error)
}

func badRequest(ctx iris.Context) {
	error := res.ErrorJs{Success: false, Error: "Bad request"}
	ctx.JSON(error)
}

func internalError(ctx iris.Context) {
	error := res.ErrorJs{Success: false, Error: "Unexpect error happened"}
	ctx.JSON(error)
}
