package controller

import (
	"github.com/go-playground/validator/v10"
	"github.com/kataras/iris/v12"
	"go-iris-example/app/api/data/req"
	"go-iris-example/app/api/data/res"
	"go-iris-example/app/api/middleware"
	"go-iris-example/app/service"
)

// -----------------[ AREA: Definitions ]-----------------//
type UserController interface {
	GetAllUsers(ctx iris.Context)
	GetUserById(ctx iris.Context)
	CreateNewUser(ctx iris.Context)
	DeleteUserById(ctx iris.Context)
	GetAllWithClassroom(ctx iris.Context)
}

type userController struct {
	userService service.UserService
}

func NewUserController(userService service.UserService) UserController {
	return &userController{userService}
}

// -----------------[ AREA: Implementations ]-----------------//
func (userCtrl userController) GetAllUsers(ctx iris.Context) {
	userEntities := userCtrl.userService.ResolveAll()
	usersJs := res.ToUsersJs(userEntities)

	ctx.JSON(res.SuccessL(usersJs))
}

func (userCtrl userController) GetUserById(ctx iris.Context) {
	id, _ := ctx.Params().GetUint("id")
	user, err := userCtrl.userService.ResolveById(id)

	if err != nil {
		middleware.CatchError(err, ctx)
		return
	}

	userJs := res.ToUserJs(*user)
	ctx.JSON(res.Success(userJs))
}

func (userCtrl userController) CreateNewUser(ctx iris.Context) {
	var userReq req.CreateUser
	err := ctx.ReadJSON(&userReq)

	if err != nil {
		if _, ok := err.(validator.ValidationErrors); ok {
			ctx.StopWithStatus(iris.StatusBadRequest)
			return
		}

		ctx.StopWithStatus(iris.StatusInternalServerError)
		return
	}

	entity := userReq.ToEntity()
	error := userCtrl.userService.Create(&entity)

	if error != nil {
		middleware.CatchError(err, ctx)
		return
	}
	ctx.StatusCode(iris.StatusCreated)
}

func (userCtrl userController) DeleteUserById(ctx iris.Context) {
	id, _ := ctx.Params().GetUint("id")
	_, err := userCtrl.userService.ResolveById(id)

	if err != nil {
		middleware.CatchError(err, ctx)
		return
	}

	userCtrl.userService.DeleteById(id)
	ctx.StatusCode(iris.StatusNoContent)
}

func (userCtrl userController) GetAllWithClassroom(ctx iris.Context) {
	resJs := userCtrl.userService.ResolveAllWithClassroom()
	ctx.JSON(res.SuccessL(resJs))
}
