package repository

import (
	"go-iris-example/app/entity"
	"gorm.io/gorm"
)

// -----------------[ AREA: Definitions ]-----------------//
type ClassroomRepository interface {
	FindAll() []entity.Classroom
}

type classroomMySQlRepository struct {
	db *gorm.DB
}

func NewClassroomRepository(db *gorm.DB) ClassroomRepository {
	return &classroomMySQlRepository{db}
}

// -----------------[ AREA: Implementations ]-----------------//
func (repo classroomMySQlRepository) FindAll() []entity.Classroom {
	var classrooms []entity.Classroom
	repo.db.Find(&classrooms)

	return classrooms
}
