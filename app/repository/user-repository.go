package repository

import (
	"errors"
	"fmt"
	"go-iris-example/app/entity"
	"go-iris-example/app/util"
	"gorm.io/gorm"
)

// -----------------[ AREA: Definitions ]-----------------//
type UserRepository interface {
	FindAll() []entity.User
	FindById(ID uint) (*entity.User, error)
	Store(user *entity.User) error
	DeleteById(ID uint) error
}

type userMySQlRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userMySQlRepository{db}
}

// -----------------[ AREA: Implementations ]-----------------//
func (repo userMySQlRepository) FindAll() []entity.User {
	var users []entity.User
	repo.db.Find(&users)

	return users
}

func (repo userMySQlRepository) FindById(ID uint) (*entity.User, error) {
	var user entity.User
	result := repo.db.Where("id = ?", ID).First(&user)

	if result.Error != nil {
		return nil, &util.EntityNotFoundErr{Message: fmt.Sprintf("Not found user with id: %d", ID)}
	}

	return &user, nil
}

func (repo userMySQlRepository) Store(user *entity.User) error {
	result := repo.db.Create(&user)

	if result.Error != nil {
		return errors.New(result.Error.Error())
	}
	return nil
}

func (repo userMySQlRepository) DeleteById(ID uint) error {
	result := repo.db.Delete(&entity.User{}, ID)
	if result.Error != nil {
		return errors.New(result.Error.Error())
	}
	return nil
}
